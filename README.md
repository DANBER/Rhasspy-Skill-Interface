# Rhasspy-Skill-Interface

Interface to use [Jaco](https://gitlab.com/Jaco-Assistant/Jaco-Master) skills with [Rhasspy](https://rhasspy.readthedocs.io/en/latest/).

<br>

**Setup**:
- Install Jaco-Master (Jaco-Satellite is optional for this)
- Choose some skills in Jaco's SkillStore
- Install and update them following Jaco's readme
- Configure this skill's config
- Generate Rhasspy's sentence files:
  ```bash
  python3 skills/skills/Rhasspy-Skill-Interface/preprocess.py
  ```
- Retrain Rhasspy
- Start Rhasspy
- Start the skill containers following Jaco's readme (Jaco-Master doesn't need to run)

<br>

**Debugging**

```bash
docker run --rm -it --name rhasspy \
  -p 12101:12101 -p 12183:12183 \
  -v "$HOME/.config/rhasspy/profiles:/profiles" \
  -v "/etc/localtime:/etc/localtime:ro" \
  --device /dev/snd:/dev/snd \
  rhasspy/rhasspy --user-profiles /profiles --profile en

docker run --network host --rm \
  --volume `pwd`/skills/skills/Rhasspy-Skill-Interface/:/Jaco-Master/skills/skills/Rhasspy-Skill-Interface/:ro \
  --volume `pwd`/skills/skills/Rhasspy-Skill-Interface/skilldata/:/Jaco-Master/skills/skills/Rhasspy-Skill-Interface/skilldata/ \
  --volume `pwd`/jacolib/:/Jaco-Master/jacolib/:ro \
  --volume `pwd`/userdata/config/:/Jaco-Master/userdata/config/:ro \
  -it master_base_image_amd64 python3 /Jaco-Master/skills/skills/Rhasspy-Skill-Interface/action_interface.py
```
