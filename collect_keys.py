import json
import os

# ==================================================================================================


filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
key_file1 = filepath + "../../../userdata/module_topic_keys.json"
key_file2 = filepath + "../../../userdata/skill_topic_keys.json"
out_file = filepath + "skilldata/topic_keys.json"

keep_containing = ["/SayText", "/UserAnswer", "/Intents/"]

# ==================================================================================================


def main():

    with open(key_file1, "r", encoding="utf-8") as file:
        keys1 = json.load(file)

    with open(key_file2, "r", encoding="utf-8") as file:
        keys2 = json.load(file)

    keys = {}
    keys.update(keys1)
    keys.update(keys2)

    keep_keys = {}
    for k, v in keys.items():
        for kc in keep_containing:
            if kc in k:
                keep_keys[k] = v
                break

    with open(out_file, "w+", encoding="utf-8") as file:
        keys2 = json.dump(keep_keys, file, indent=2)


# ==================================================================================================

if __name__ == "__main__":
    main()
