import os
import subprocess

import collect_keys

import generate_sentences
from jacolib import assistant

# ==================================================================================================


def copy_sentences():

    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    assist = assistant.Assistant(repo_path=filepath)
    config = assist.get_config()

    rp_path = config["user"]["rhasspy_profile_path"]
    if rp_path != "":

        src_path = os.path.abspath(filepath + "output/sentences.ini")
        savepath = os.path.join(rp_path, "sentences.ini")

        # Copy with system call, because shutil didn't work with '~' home directory path
        cmd = "cp {} {}".format(src_path, savepath)
        with subprocess.Popen(["/bin/bash", "-c", cmd]) as sp:
            sp.wait()

        src_path = os.path.abspath(filepath + "output/slots/")
        savepath = os.path.join(rp_path, "slots/")

        cmd = "rm -r {} ; cp -a {} {}".format(savepath, src_path, savepath)
        with subprocess.Popen(["/bin/bash", "-c", cmd]) as sp:
            sp.wait()


# ==================================================================================================


def main():
    collect_keys.main()
    generate_sentences.main()
    copy_sentences()


# ==================================================================================================

if __name__ == "__main__":
    main()
