import json
import os
import time
from typing import Any

from paho.mqtt.client import Client, MQTTMessage

from jacolib import assistant, comm_tools, extra_tools

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
assist: assistant.Assistant
mqtt_client: Client

rhasspy_intent_topic = "hermes/nlu/intentParsed"
rhasspy_end_session_topic = "hermes/dialogueManager/endSession"
rhasspy_continue_session_topic = "hermes/dialogueManager/continueSession"
rhasspy_say_topic = "hermes/tts/say"

question_asked = False
key_file = filepath + "skilldata/topic_keys.json"

# ==================================================================================================


def on_connect(client: Client) -> None:
    print("Connected to Rhasspy")
    pass


def on_message(
    client: Client,  # pylint: disable=unused-argument
    userdata: Any,  # pylint: disable=unused-argument
    msg: MQTTMessage,
) -> None:
    print("\nReceived message on topic:", msg.topic)

    if msg.topic == rhasspy_intent_topic:
        new_intent_callback(msg.payload)


# ==================================================================================================


def new_intent_callback(payload):
    global question_asked

    payload = json.loads(payload)
    print("Recieved a new intent:", payload)

    skill_topic = extra_tools.intent_to_topic(payload["intent"]["intentName"])
    sat = {"siteId": payload["siteId"], "sessionId": payload["sessionId"]}
    satellite = json.dumps(sat)

    pld_out = {
        "intent": {"name": extra_tools.topic_to_intent(skill_topic)},
        "text": payload["input"],
        "greedy": "",
        "entities": [
            {
                # Rhasspy didn't seem to support named entities
                "entity": v["entity"],
                "value": v["value"]["value"],
            }
            for v in payload["slots"]
        ],
        "satellite": satellite,
        "timestamp": time.time(),
    }
    msg = comm_tools.encrypt_msg(pld_out, skill_topic)

    if question_asked:
        # The question answer needs to be sent to a different topic
        question_asked = False

        pld_out = {
            "payload": msg.decode(),
            "topic": skill_topic,
        }
        skill_topic = "Jaco/Skills/UserAnswer"

        msg = comm_tools.encrypt_msg(pld_out, skill_topic)

    assist.mqtt_client.publish(skill_topic, msg)
    print("Sent the intent: {} to topic: '{}'".format(pld_out, skill_topic))


# ==================================================================================================


def callback_say_text(payload):
    global question_asked

    print("Recieved skill answer:", payload)

    if "question_intents" in payload:
        # Continue session with question
        topic_out = rhasspy_continue_session_topic
        question_asked = True
        pld_out = {
            "sessionId": json.loads(payload["satellite"])["sessionId"],
            "intentFilter": payload["question_intents"],
            "text": payload["data"],
        }

        msg = json.dumps(pld_out)
        mqtt_client.publish(topic_out, msg)
        print("Sent the answer: {} to topic: '{}'".format(pld_out, topic_out))

    else:
        # End session with out text and send the text directly to the TTS service
        # This is needed for apps that send another text after some delay
        topic_out = rhasspy_end_session_topic
        pld_out = {
            "sessionId": json.loads(payload["satellite"])["sessionId"],
            "text": "",
        }
        msg = json.dumps(pld_out)
        mqtt_client.publish(topic_out, msg)

        topic_out = rhasspy_say_topic
        pld_out = {
            "siteId": json.loads(payload["satellite"])["siteId"],
            "text": payload["data"],
        }
        msg = json.dumps(pld_out)
        mqtt_client.publish(topic_out, msg)


# ==================================================================================================


def main():
    global assist, mqtt_client

    assist = assistant.Assistant(repo_path=filepath)
    config = assist.get_config()

    connect_config = {
        "mqtt_broker_address": config["user"]["rhasspy_mqtt_broker_address"],
        "mqtt_username": config["user"]["rhasspy_mqtt_username"],
        "mqtt_password": config["user"]["rhasspy_mqtt_password"],
    }
    mqtt_client = comm_tools.connect_mqtt_client(connect_config, on_connect, on_message)
    mqtt_client.subscribe(rhasspy_intent_topic)

    with open(key_file, "r", encoding="utf-8") as file:
        keys = json.load(file)
    topics = list(keys.keys())

    # Allow access to topics
    assist.config["system"]["topics_read"] = topics

    # Add a callback to all say topics
    for top in topics:
        if "/SayText" in top:
            assist.add_topic_callback(top, callback_say_text)

    while True:
        mqtt_client.loop()
        assist.mqtt_client.loop()
        time.sleep(0.1)


# ==================================================================================================

if __name__ == "__main__":
    main()
