import json
import os
import re

from jacolib import utils

# ==================================================================================================


filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
nlu_file = filepath + "../../generated/nlu/nlu.json"

outputdir = filepath + "output/"
slotdir = outputdir + "slots/"
senfile = outputdir + "sentences.ini"

# ==================================================================================================


def main():

    with open(nlu_file, "r", encoding="utf-8") as file:
        nludata = json.load(file)

    utils.empty_with_ignore(outputdir)
    os.makedirs(slotdir)

    for lookup in nludata["lookups"]:
        content = []
        for item in nludata["lookups"][lookup]:
            item = item.replace("->", ":")
            content.append(item)

        with open(slotdir + lookup, "w+", encoding="utf-8") as file:
            text = "\n".join(content) + "\n"
            file.write(text)

    text = ""
    for intent in nludata["intents"]:
        text += "[{}]\n".format(intent)

        content = []
        for item in nludata["intents"][intent]:
            item = re.sub(r"\[---\]\(([^\)]*)\)", r"($\1){\1}", item)
            item = re.sub(r"\?[^\)\(]*\)", ")", item)
            item = re.sub(r"\{([^\}]*)(\?[^\}]*)\}", r"{\1}", item)
            content.append(item)
        text += "\n".join(content) + "\n\n"

    text = text[:-1]
    with open(senfile, "w+", encoding="utf-8") as file:
        file.write(text)


# ==================================================================================================

if __name__ == "__main__":
    main()
